"""
TASC_Learner.py
"""

import tensorflow as tf
import numpy as np
from tqdm import trange
import math
import _pickle as pkl
import os
from utils import rand_vectors

SESS = tf.Session()
TRAINING = tf.placeholder_with_default(False, (), 'training')

DT = 0.05
N = 100

PRT_STATE_SIZE = 2
SWM1_STATE_SIZE = 2
SWM2_STATE_SIZE = 2
SWM1_ACTIN_SIZE = 1
SWM2_ACTIN_SIZE = 1
SWM1_PARAM_SIZE = 1
SWM2_PARAM_SIZE = 1
SWM_MAXIM_SIZE = 16

Q_PRT = np.array([[10.0, 0.0], [0.0, 1.0]])
R_SWM1 = np.array([[0.1]])
R_SWM2 = np.array([[0.1]])
GAMMA_ABS = 1.0
GAMMA_SWM = 1.0
GAMMA_BND = 1.0

ABS_STATE_SIZE = 8

ABS_TRN_H_SIZE = 100
ABS_CMB_H_SIZE = 100
ABS_DYN_H_SIZE = 100
PRT_POL_H_SIZE = 100
SWM_POL_H_SIZE = 100

LR = 0.001
L2_GAIN = 0.1
USE_BIAS = True
DROPOUT_RATE = 0.2

N_EPOCH = 100
N_BATCH = 1000
BAT_SIZE = 128
VAL_SIZE = 32
VAL_BLOK = 16
SWM_SIZE_UPDATE_RATE = 50

PATIENCE = 10

HOME = '../../data/MFRMFP/results/NoHomo/'
LOGS = '../../data/MFRMFP/logs/NoHomo/'
AUTOSAVE_FILENAME = 'autosave_epoch%d.pkl'
VAL_RSLT_FILENAME = 'val_data_epoch%d.pkl'
HST_RSLT_FILENAME = 'hist_data.pkl'
FINAL_FILENAME = 'final_weights.pkl'

class Abs_Trn(object):
    def __init__(self, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                ABS_TRN_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                ABS_STATE_SIZE,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None
    def __call__(self, s_si, p_si):
        with tf.variable_scope('AbsTrn' if self.scope is None else self.scope) as scope:
            comb_in = tf.concat([s_si, p_si], axis=2)
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Abs_Cmb(object):
    def __init__(self, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                ABS_CMB_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                ABS_STATE_SIZE,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None
    def __call__(self, s_a1, s_a2):
        with tf.variable_scope('AbsCmb' if self.scope is None else self.scope) as scope:
            comb_in = tf.concat((s_a1, s_a2), axis=1)
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Abs_Enc(object):
    def __init__(self, lock=False):
        self.abs_trn1 = Abs_Trn(lock)
        self.abs_trn2 = Abs_Trn(lock)
        self.abs_cmb = Abs_Cmb(lock)
        self.scope = None

    def __call__(self, s_s1, s_s2, p_s1, p_s2, swm_size):
        size1 = tf.shape(s_s1)[1]
        size2 = tf.shape(s_s2)[1]
        padding = [[0, 0], [0, swm_size-size1-size2], [0, 0]]

        with tf.variable_scope('AbsEnc' if self.scope is None else self.scope) as scope:
            with tf.variable_scope('Swm1'):
                s_a1 = self.abs_trn1(s_s1, p_s1)
            with tf.variable_scope('Swm2'):
                s_a2 = self.abs_trn2(s_s2, p_s2)
            s_a = tf.concat((s_a1, s_a2), axis=1)
            s_a = tf.pad(s_a, padding, name='s_a_padded')
            if self.scope is None:
                self.scope = scope

        s_a_list = []
        for i in range(swm_size):
            s_a_list.append(s_a[:, i, :])

        s_a_tree = self.binary_tree_build([s_a_list])
        return s_a_tree[-1][0]

    def binary_tree_build(self, s_a):
        grp1_abs = s_a[-1][0::2]
        grp2_abs = s_a[-1][1::2]

        size_grp1 = len(grp1_abs)
        size_grp2 = len(grp2_abs)

        s_a.append([])

        for i in range(size_grp2):
            with tf.variable_scope(self.scope):
                s_a[-1].append(self.abs_cmb(grp1_abs[i], grp2_abs[i]))

        if size_grp1 != size_grp2:
            s_a.append(grp2_abs[-1])

        if len(s_a[-1]) == 1:
            return s_a
        else:
            return self.binary_tree_build(s_a)

class Abs_Dyn(object):
    def __init__(self, lock=False):
        self.dof = int(PRT_STATE_SIZE/2)
        self.dt = tf.constant(DT, dtype='float')
        self.h_layer = tf.keras.layers.Dense(
                ABS_DYN_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                self.dof,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None

    def __call__(self, s_p, s_a):
        with tf.variable_scope('AbsDyn' if self.scope is None else self.scope) as scope:
            comb_in = tf.concat((s_p, s_a), axis=1)
            p_old = s_p[:, 0:self.dof]
            v_old = s_p[:, self.dof:]
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            a_new = self.o_layer(h)
            v_new = v_old + self.dt*a_new
            p_new = p_old + self.dt*v_new
            o = tf.concat((p_new, v_new), axis=1)
            if self.scope is None:
                self.scope = scope
        return o

class Prt_Pol(object):
    def __init__(self, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                PRT_POL_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                ABS_STATE_SIZE,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None

    def __call__(self, s_p):
        with tf.variable_scope('PrtPol' if self.scope is None else self.scope) as scope:
            h = self.h_layer(s_p)
            h = self.dropout(h, training=TRAINING)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Swm_Pol(object):
    def __init__(self, actin_size, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                SWM_POL_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                actin_size,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None

    def __call__(self, s_s, s_a, s_d, p_s):
        with tf.variable_scope('SwmPol' if self.scope is None else self.scope) as scope:
            err = tf.tile(tf.expand_dims(s_a-s_d, axis=1), (1, tf.shape(s_s)[1], 1))
            comb_in = tf.concat((s_s, err, p_s), axis=2)
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            a_s = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return a_s

class System(object):
    theta_min = -np.pi/6.0
    theta_max = np.pi/6.0
    omega_min = -1.0
    omega_max = 1.0
    p_min = -0.375
    p_max = 0.375
    v_min = -1.0
    v_max = 1.0
    m_min = 1.5
    m_max = 2.0
    J = 1.5
    gamma_1 = 0.07
    gamma_2 = 1000
    gamma_3 = 700
    gamma_4 = 0.015
    gamma_5 = 1000
    gamma_6 = 0.5
    g = 9.81
    c = 1.5

    def __init__(self):
        with tf.variable_scope('DynSystem') as scope:
            self.J_tf = tf.constant(self.J, dtype='float')
            self.gamma_1_tf = tf.constant(self.gamma_1, dtype='float')
            self.gamma_2_tf = tf.constant(self.gamma_2, dtype='float')
            self.gamma_3_tf = tf.constant(self.gamma_3, dtype='float')
            self.gamma_4_tf = tf.constant(self.gamma_4, dtype='float')
            self.gamma_5_tf = tf.constant(self.gamma_5, dtype='float')
            self.gamma_6_tf = tf.constant(self.gamma_6, dtype='float')
            self.g_tf = tf.constant(self.g, dtype='float')
            self.c_tf = tf.constant(self.c, dtype='float')
            self.dt = tf.constant(DT, 'float')
            self.scope = scope

    def gen_prt_states(self, n):
        return rand_vectors(n, np.array([self.theta_min, self.omega_min]), np.array([self.theta_max, self.omega_max]))

    def gen_swm1_states(self, n, swm_size):
        return rand_vectors((n, swm_size), np.array([self.p_min, 0.0]), np.array([self.p_max, 0.0]))

    def gen_swm2_states(self, n, swm_size):
        return rand_vectors((n, swm_size), np.array([self.p_min, self.v_min]), np.array([self.p_max, self.v_max]))

    def gen_swm1_params(self, n, swm_size):
        return rand_vectors((n, swm_size), np.array([self.m_min]), np.array([self.m_max]))

    def gen_swm2_params(self, n, swm_size):
        return rand_vectors((n, swm_size), np.array([self.m_min]), np.array([self.m_max]))

    def strbk_friction(self, v):
        return self.gamma_1_tf*(tf.tanh(self.gamma_2_tf*v) - tf.tanh(self.gamma_3_tf)*v) + self.gamma_4_tf*tf.tanh(self.gamma_5_tf*v) + self.gamma_6_tf*v

    def prt_dyn_cont(self, s_p, s_s1, s_s2, p_s1, p_s2):
        m1 = p_s1[:, :, 0]
        m2 = p_s2[:, :, 0]
        J_eff = self.J_tf + tf.reduce_sum(m1*(s_s1[:, :, 0]**2), axis=1) + tf.reduce_sum(m2*(s_s2[:, :, 0]**2), axis=1)
        tau = tf.reduce_sum(m1*s_s1[:, :, 0], axis=1) + tf.reduce_sum(m2*s_s2[:, :, 0], axis=1)
        J_dot = tf.reduce_sum(m1*(s_s1[:, :, 0]*s_s1[:, :, 1]), axis=1) + tf.reduce_sum(m2*(s_s2[:, :, 0]*s_s2[:, :, 1]), axis=1)
        theta_dot = s_p[:, 1]
        omega_dot = (-self.g_tf*tf.cos(s_p[:, 0])*tau - s_p[:, 1]*J_dot - self.strbk_friction(s_p[:, 1]))/J_eff
        return tf.stack((theta_dot, omega_dot), axis=1)

    def swm1_dyn_cont(self, s_s, a_s, p_s):
        p_dot = a_s[:, :, 0]
        v_dot = (a_s[:, :, 0] - s_s[:, :, 1])/DT
        return tf.stack((p_dot, v_dot), axis=2)

    def swm2_dyn_cont(self, s_s, a_s, p_s):
        m = p_s[:, :, 0]
        p_dot = s_s[:, :, 1]
        v_dot = (a_s[:, :, 0] - self.c_tf*s_s[:, :, 1])/m
        return tf.stack((p_dot, v_dot), axis=2)

    def dyn_RK4(self, s_p_old, s_s1_old, s_s2_old, a_s1_old, a_s2_old, p_s1, p_s2):
        with tf.variable_scope(self.scope):
            s_p_dot1 = self.prt_dyn_cont(s_p_old, s_s1_old, s_s2_old, p_s1, p_s2)
            s_s1_dot1 = self.swm1_dyn_cont(s_s1_old, a_s1_old, p_s1)
            s_s2_dot1 = self.swm2_dyn_cont(s_s2_old, a_s2_old, p_s2)
            s_p_dot2 = self.prt_dyn_cont(s_p_old+0.5*self.dt*s_p_dot1, s_s1_old+0.5*self.dt*s_s1_dot1, s_s2_old+0.5*self.dt*s_s2_dot1, p_s1, p_s2)
            s_s1_dot2 = self.swm1_dyn_cont(s_s1_old+0.5*self.dt*s_s1_dot1, a_s1_old, p_s1)
            s_s2_dot2 = self.swm2_dyn_cont(s_s2_old+0.5*self.dt*s_s2_dot1, a_s2_old, p_s2)
            s_p_dot3 = self.prt_dyn_cont(s_p_old+0.5*self.dt*s_p_dot2, s_s1_old+0.5*self.dt*s_s1_dot2, s_s2_old+0.5*self.dt*s_s2_dot2, p_s1, p_s2)
            s_s1_dot3 = self.swm1_dyn_cont(s_s1_old+0.5*self.dt*s_s1_dot2, a_s1_old, p_s1)
            s_s2_dot3 = self.swm2_dyn_cont(s_s2_old+0.5*self.dt*s_s2_dot2, a_s2_old, p_s2)
            s_p_dot4 = self.prt_dyn_cont(s_p_old+self.dt*s_p_dot3, s_s1_old+self.dt*s_s1_dot3, s_s2_old+self.dt*s_s2_dot3, p_s1, p_s2)
            s_s1_dot4 = self.swm1_dyn_cont(s_s1_old+self.dt*s_s1_dot3, a_s1_old, p_s1)
            s_s2_dot4 = self.swm2_dyn_cont(s_s2_old+self.dt*s_s2_dot3, a_s2_old, p_s2)

            s_p_new = s_p_old + self.dt/6.0*(s_p_dot1 + 2*s_p_dot2 + 2*s_p_dot3 + s_p_dot4)
            s_s1_new = s_s1_old + self.dt/6.0*(s_s1_dot1 + 2*s_s1_dot2 + 2*s_s1_dot3 + s_s1_dot4)
            s_s2_new = s_s1_old + self.dt/6.0*(s_s2_dot1 + 2*s_s2_dot2 + 2*s_s2_dot3 + s_s2_dot4)

        return s_p_new, s_s1_new, s_s2_new

    def dyn_SIE(self, s_p_old, s_s1_old, s_s2_old, a_s1, a_s2, p_s1, p_s2):
        theta_old = s_p_old[:, 0]
        omega_old = s_p_old[:, 1]

        p1_old = s_s1_old[:, :, 0]
        v1_old = s_s1_old[:, :, 1]
        p2_old = s_s2_old[:, :, 0]
        v2_old = s_s2_old[:, :, 1]

        v1_new = a_s1[:, :, 0]
        f2 = a_s2[:, :, 0]
        m1 = p_s1[:, :, 0]
        m2 = p_s2[:, :, 0]

        tau = tf.reduce_sum(m1*p1_old, axis=1) + tf.reduce_sum(m2*p2_old, axis=1)
        J = tf.reduce_sum(m1*p1_old**2, axis=1) + tf.reduce_sum(m2*p2_old**2, axis=1) + self.J_tf
        J_dot = tf.reduce_sum(2*m1*p1_old*v1_old, axis=1) + tf.reduce_sum(2*m2*p2_old*v2_old, axis=1)

        alpha_new = (-self.g_tf*tf.cos(theta_old)*tau - omega_old*J_dot - self.strbk_friction(omega_old))/J
        omega_new = omega_old + self.dt*alpha_new
        theta_new = theta_old + self.dt*omega_new

        p1_new = p1_old + self.dt*v1_new
        a2_new = (f2 - self.c_tf*v2_old)/m2
        v2_new = v2_old + self.dt*a2_new
        p2_new = p2_old + self.dt*v2_new

        s_p_new = tf.stack((theta_new, omega_new), axis=1)
        s_s1_new = tf.stack((p1_new, v1_new), axis=2)
        s_s2_new = tf.stack((p2_new, v2_new), axis=2)

        return s_p_new, s_s1_new, s_s2_new

    def __call__(self, s_p_old, s_s1_old, s_s2_old, a_s1_old, a_s2_old, p_s1, p_s2):
        return self.dyn_SIE(s_p_old, s_s1_old, s_s2_old, a_s1_old, a_s2_old, p_s1, p_s2)


class Iteration(object):
    def __init__(self, abs_enc, abs_dyn, prt_pol, swm1_pol, swm2_pol, sys):
        self.abs_enc = abs_enc
        self.abs_dyn = abs_dyn
        self.prt_pol = prt_pol
        self.swm1_pol = swm1_pol
        self.swm2_pol = swm2_pol
        self.sys = sys

    def __call__(self, s_p_old, s_s1_old, s_s2_old, p_s1, p_s2, swm_size):
        with tf.variable_scope('Iter'):
            s_a_old = self.abs_enc(s_s1_old, s_s2_old, p_s1, p_s2, swm_size)

            s_d_old = self.prt_pol(s_p_old)
            with tf.variable_scope('Swm1'):
                a_s1_old = self.swm1_pol(s_s1_old, s_a_old, s_d_old, p_s1)
            with tf.variable_scope('Swm2'):
                a_s2_old = self.swm2_pol(s_s2_old, s_a_old, s_d_old, p_s2)

            s_p_new, s_s1_new, s_s2_new = self.sys(s_p_old, s_s1_old, s_s2_old, a_s1_old, a_s2_old, p_s1, p_s2)

            s_p_prd = self.abs_dyn(s_p_old, s_a_old)

        return s_p_new, s_s1_new, s_s2_new, s_a_old, s_d_old, a_s1_old, a_s2_old, s_p_prd

class TASC_trainer(object):
    def __init__(self):
        self.abs_enc = Abs_Enc()
        self.abs_dyn = Abs_Dyn()
        self.prt_pol = Prt_Pol()
        self.swm1_pol = Swm_Pol(SWM1_ACTIN_SIZE)
        self.swm2_pol = Swm_Pol(SWM2_ACTIN_SIZE)
        self.sys = System()
        self.iter = Iteration(self.abs_enc, self.abs_dyn, self.prt_pol, self.swm1_pol, self.swm2_pol, self.sys)
        self.loss_scope = None
        self.build()

    def build(self):
        with tf.variable_scope('TASC_Learner') as scope:
            self.scope = scope
            self.s_p_0 = tf.placeholder('float', (None, PRT_STATE_SIZE), 's_p_0')
            self.s_s1_0 = tf.placeholder('float', (None, None, SWM1_STATE_SIZE), 's_s_0')
            self.s_s2_0 = tf.placeholder('float', (None, None, SWM2_STATE_SIZE), 's_s_0')
            self.p_s1 = tf.placeholder('float', (None, None, SWM1_PARAM_SIZE), 'p_s')
            self.p_s2 = tf.placeholder('float', (None, None, SWM2_PARAM_SIZE), 'p_s')

            Q_p = tf.constant(Q_PRT, dtype='float')
            R_s1 = tf.constant(R_SWM1, dtype='float')
            R_s2 = tf.constant(R_SWM2, dtype='float')
            L  = tf.constant((self.sys.p_max - self.sys.p_min), dtype='float')

            self.outputs = []
            self.losses = []
            self.trainers = []
            self.train_ops = []

            self.swm_sizes = [2**(i+1) for i in range(int(math.ceil(math.log(SWM_MAXIM_SIZE, 2))))]

            for swm_size in self.swm_sizes:
                print('building network for swarm size %d'%swm_size)
                s_p = [self.s_p_0]
                s_s1 = [self.s_s1_0]
                s_s2 = [self.s_s2_0]
                s_a = []
                s_d = []
                a_s1 = []
                a_s2 = []
                l_abs = []
                l_swm = []
                l_prt = []
                l_bnd = []

                for i in trange(N):
                    s_p_nxt, s_s1_nxt, s_s2_nxt, s_a_cur, s_d_cur, a_s1_cur, a_s2_cur, s_p_prd = self.iter(s_p[i], s_s1[i], s_s2[i], self.p_s1, self.p_s2, swm_size)
                    s_p.append(s_p_nxt)
                    s_s1.append(s_s1_nxt)
                    s_s2.append(s_s2_nxt)
                    s_a.append(s_a_cur)
                    s_d.append(s_d_cur)
                    a_s1.append(a_s1_cur)
                    a_s2.append(a_s2_cur)

                    with tf.variable_scope('loss' if self.loss_scope is None else self.loss_scope) as scope:
                        l_abs.append(GAMMA_ABS*tf.reduce_sum(tf.squared_difference(s_p_nxt, s_p_prd), axis=1))
                        l_swm.append(GAMMA_SWM*tf.reduce_sum(tf.squared_difference(s_a_cur, s_d_cur), axis=1) + tf.einsum('ijk,kl,ijl->i', a_s1_cur, R_s1, a_s1_cur) + tf.einsum('ijk,kl,ijl->i', a_s2_cur, R_s2, a_s2_cur))
                        l_prt.append(tf.einsum('ij,jk,ik->i', s_p_nxt, Q_p, s_p_nxt))
                        l_bnd.append(GAMMA_BND*(tf.reduce_sum(tf.square(tf.maximum(0.0, tf.abs(s_s1_nxt[:, :, 0])-L/2.0)), axis=1)+tf.reduce_sum(tf.square(tf.maximum(0.0, tf.abs(s_s2_nxt[:, :, 0])-L/2.0)), axis=1)))

                        if self.loss_scope is None:
                            self.loss_scope = scope

                with tf.variable_scope('outputs'):
                    self.outputs.append({})
                    self.outputs[-1]['s_p'] = tf.stack(s_p, axis=1)
                    self.outputs[-1]['s_s1'] = tf.stack(s_s1, axis=1)
                    self.outputs[-1]['s_s2'] = tf.stack(s_s2, axis=1)
                    self.outputs[-1]['s_a'] = tf.stack(s_a, axis=1)
                    self.outputs[-1]['s_d'] = tf.stack(s_d, axis=1)
                    self.outputs[-1]['a_s1'] = tf.stack(a_s1, axis=1)
                    self.outputs[-1]['a_s2'] = tf.stack(a_s2, axis=1)

                with tf.variable_scope(self.loss_scope):
                    self.outputs[-1]['l_abs'] = tf.stack(l_abs, axis=1)
                    self.outputs[-1]['l_swm'] = tf.stack(l_swm, axis=1)
                    self.outputs[-1]['l_prt'] = tf.stack(l_prt, axis=1)
                    self.outputs[-1]['l_bnd'] = tf.stack(l_bnd, axis=1)

                    self.losses.append({})
                    self.losses[-1]['L_abs'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_abs'], axis=1))
                    self.losses[-1]['L_swm'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_swm'], axis=1))
                    self.losses[-1]['L_prt'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_prt'], axis=1))
                    self.losses[-1]['L_bnd'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_bnd'], axis=1))

                    total_loss = self.losses[-1]['L_abs'] + self.losses[-1]['L_swm'] + self.losses[-1]['L_prt'] + self.losses[-1]['L_bnd'] + tf.losses.get_regularization_loss()

                with tf.variable_scope('train', reuse=tf.AUTO_REUSE):
                    self.trainers.append(tf.train.AdamOptimizer(learning_rate=LR))
                    self.train_ops.append(self.trainers[-1].minimize(total_loss))

    def train(self):
        hist = {}

        SESS.run(tf.global_variables_initializer())

        for key in self.losses[0].keys():
            hist['ave_%s'%key] = np.zeros(N_EPOCH)
            hist['val_%s'%key] = np.zeros(N_EPOCH)

        min_weights=[]
        min_loss = math.inf
        min_loss_epoch = -1
        min_count = 0
        var_list = tf.trainable_variables()

        val_blocks = []
        for swm_size_i in range(len(self.swm_sizes)):
            for i in range(VAL_BLOK):
                val_blocks.append({})
                swm_size_total = np.random.randint(2**swm_size_i+1, 2**(swm_size_i+1)+1)
                swm_size_split = np.random.randint(1, swm_size_total)
                swm1_size = swm_size_split
                swm2_size = swm_size_total - swm_size_split
                val_blocks[-1]['s_p_0'] = self.sys.gen_prt_states(VAL_SIZE)
                val_blocks[-1]['s_s1_0'] = self.sys.gen_swm1_states(VAL_SIZE, swm1_size)
                val_blocks[-1]['s_s2_0'] = self.sys.gen_swm2_states(VAL_SIZE, swm2_size)
                val_blocks[-1]['p_s1'] = self.sys.gen_swm1_params(VAL_SIZE, swm1_size)
                val_blocks[-1]['p_s2'] = self.sys.gen_swm2_params(VAL_SIZE, swm2_size)
                val_blocks[-1]['swm_i'] = swm_size_i
                val_blocks[-1]['feed_dict'] = {self.s_p_0:val_blocks[-1]['s_p_0'], self.s_s1_0:val_blocks[-1]['s_s1_0'], self.s_s2_0:val_blocks[-1]['s_s2_0'], self.p_s1:val_blocks[-1]['p_s1'], self.p_s2:val_blocks[-1]['p_s2'], TRAINING:False}

        for epoch in range(N_EPOCH):
            print('Epoch: %d/%d'%(epoch+1, N_EPOCH))
            batches = trange(N_BATCH)
            for batch in batches:
                if batch%SWM_SIZE_UPDATE_RATE == 0:
                    swm_size_i = np.random.randint(len(self.swm_sizes))
                swm_size_total = np.random.randint(2**swm_size_i+1, 2**(swm_size_i+1)+1)
                swm_size_split = np.random.randint(1, swm_size_total)
                swm1_size = swm_size_split
                swm2_size = swm_size_total - swm_size_split
                s_p_0_bat = self.sys.gen_prt_states(BAT_SIZE)
                s_s1_0_bat = self.sys.gen_swm1_states(BAT_SIZE, swm1_size)
                s_s2_0_bat = self.sys.gen_swm2_states(BAT_SIZE, swm2_size)
                p_s1_bat = self.sys.gen_swm1_params(BAT_SIZE, swm1_size)
                p_s2_bat = self.sys.gen_swm2_params(BAT_SIZE, swm2_size)
                feed_dict_bat = {self.s_p_0:s_p_0_bat, self.s_s1_0:s_s1_0_bat, self.s_s2_0:s_s2_0_bat, self.p_s1:p_s1_bat, self.p_s2:p_s2_bat, TRAINING:True}

                losses, _ = SESS.run([self.losses[swm_size_i], self.train_ops[swm_size_i]], feed_dict=feed_dict_bat)

                msg=''
                for key in self.losses[swm_size_i].keys():
                    hist['ave_%s'%key][epoch] += losses[key]
                    msg += '(ave_%s: %f)'%(key, hist['ave_%s'%key][epoch]/(batch+1))

                batches.set_description(msg)

            val_losses = []
            val_outputs = []
            for block in val_blocks:
                l, o = SESS.run([self.losses[block['swm_i']], self.outputs[block['swm_i']]], feed_dict=block['feed_dict'])
                val_losses.append(l)
                val_outputs.append(o)

            total_loss = 0
            msg = ''
            for key in self.losses[0].keys():
                hist['ave_%s'%key][epoch] /= N_BATCH
                for val_loss in val_losses:
                    hist['val_%s'%key][epoch] += val_loss[key]
                hist['val_%s'%key][epoch] /= len(val_losses)
                total_loss += hist['val_%s'%key][epoch]
                msg += '(%s Ave: %f, Val: %f)'%(key, hist['ave_%s'%key][epoch], hist['val_%s'%key][epoch])

            print('Epoch %d Complete! %s'%(epoch+1, msg))

            if total_loss < min_loss:
                min_loss = total_loss
                min_weights = SESS.run(var_list)
                min_count = 0
                min_loss_epoch = epoch
                if AUTOSAVE_FILENAME is not None:
                    self.save(AUTOSAVE_FILENAME%epoch, to_save=(var_list, min_weights))
                if VAL_RSLT_FILENAME is not None:
                    s_p_0 = []
                    s_s1_0 = []
                    s_s2_0 = []
                    p_s1 = []
                    p_s2 = []
                    for block in val_blocks:
                        s_p_0.append(block['s_p_0'])
                        s_s1_0.append(block['s_s1_0'])
                        s_s2_0.append(block['s_s2_0'])
                        p_s1.append(block['p_s1'])
                        p_s2.append(block['p_s2'])
                    pkl.dump([val_outputs, val_losses, s_p_0, s_s1_0, s_s2_0, p_s1, p_s2], open(HOME+VAL_RSLT_FILENAME%epoch, 'wb'))
            else:
                min_count += 1

            pkl.dump(hist, open(HOME+HST_RSLT_FILENAME, 'wb'))

            if min_count == PATIENCE:
                print('Stopping Training, validation loss has stopped decreasing')
                for key in self.losses[0].keys():
                    hist['ave_%s'%key] = hist['ave_%s'%key][:epoch+1]
                    hist['val_%s'%key] = hist['val_%s'%key][:epoch+1]
                break

        SESS.run([var.assign(val) for var, val in zip(var_list, min_weights)])
        hist['min_loss_epoch'] = min_loss_epoch
        return hist

    def save(self, filename, to_save=None):
        if to_save is None:
            var_list = tf.trainable_variables()
            val_list = SESS.run(var_list)
        else:
            var_list, val_list = to_save

        os.makedirs(HOME, exist_ok=True)
        pkl.dump({var.name:val for var, val in zip(var_list, val_list)}, open(HOME+filename, 'wb'))

    def load(self, filename):
        var_list = tf.trainable_variables()
        vals = pkl.load(open(HOME+filename, 'rb'))

        SESS.run([var.assign(vals[var.name]) for var in var_list])

if __name__ == '__main__':
    os.makedirs(HOME, exist_ok=True)
    os.makedirs(LOGS, exist_ok=True)
    print('Setting up networks...')
    learner = TASC_trainer()

    #print('Logging...')
    #tf.summary.FileWriter(LOGS, SESS.graph)

    print('Training...')
    hist = learner.train()

    print('Saving results')
    learner.save(FINAL_FILENAME)
