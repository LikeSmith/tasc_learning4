"""
TASC_Learner.py
"""

import tensorflow as tf
import numpy as np
from tqdm import trange
import math
import _pickle as pkl
import os
from utils import rand_vectors

SESS = tf.Session()
TRAINING = tf.placeholder_with_default(False, (), 'training')

DT = 0.05
N = 100

PRT_STATE_SIZE = 2
SWM_STATE_SIZE = 2
SWM_ACTIN_SIZE = 1
SWM_PARAM_SIZE = 1
SWM_MAXIM_SIZE = 16

Q_PRT = np.array([[10.0, 0.0], [0.0, 1.0]])
R_SWM = np.array([[0.1]])
GAMMA_ABS = 1.0
GAMMA_SWM = 1.0
GAMMA_BND = 1.0

ABS_STATE_SIZE = 8

ABS_TRN_H_SIZE = 100
ABS_CMB_H_SIZE = 100
ABS_DYN_H_SIZE = 100
PRT_POL_H_SIZE = 100
SWM_POL_H_SIZE = 100

LR = 0.001
L2_GAIN = 0.1
USE_BIAS = False
DROPOUT_RATE = 0.2

N_EPOCH = 100
N_BATCH = 1000
BAT_SIZE = 128
VAL_SIZE = 32
VAL_BLOK = 16
SWM_SIZE_UPDATE_RATE = 50

PATIENCE = 10

HOME = '../../data/MFRMFP/results/DI/'
LOGS = '../../data/MFRMFP/logs/DI/'
AUTOSAVE_FILENAME = 'autosave_epoch%d.pkl'
VAL_RSLT_FILENAME = 'val_data_epoch%d.pkl'
HST_RSLT_FILENAME = 'hist_data.pkl'
FINAL_FILENAME = 'final_weights.pkl'

class Abs_Trn(object):
    def __init__(self, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                ABS_TRN_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                ABS_STATE_SIZE,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None
    def __call__(self, s_si, p_si):
        with tf.variable_scope('AbsTrn' if self.scope is None else self.scope) as scope:
            comb_in = tf.concat([s_si, p_si], axis=1)
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Abs_Cmb(object):
    def __init__(self, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                ABS_CMB_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                ABS_STATE_SIZE,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None
    def __call__(self, s_a1, s_a2):
        with tf.variable_scope('AbsCmb' if self.scope is None else self.scope) as scope:
            comb_in = tf.concat((s_a1, s_a2), axis=1)
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Abs_Enc(object):
    def __init__(self, lock=False):
        self.abs_trn = Abs_Trn(lock)
        self.abs_cmb = Abs_Cmb(lock)
        self.scope = None
    def __call__(self, s_s, p_s, swm_size):
        padding = [[0, 0], [0, swm_size-tf.shape(s_s)[1]], [0, 0]]
        s_s = tf.pad(s_s, padding, name='s_s_padded')
        p_s = tf.pad(p_s, padding, name='p_s_padded')
        s_a = [[]]
        with tf.variable_scope('AbsEnc' if self.scope is None else self.scope) as scope:
            for i in range(swm_size):
                s_a[0].append(self.abs_trn(s_s[:, i, :], p_s[:, i, :]))
            if self.scope is None:
                self.scope = scope
        s_a= self.binary_tree_build(s_a)
        return s_a[-1][0]

    def binary_tree_build(self, s_a):
        grp1_abs = s_a[-1][0::2]
        grp2_abs = s_a[-1][1::2]

        size_grp1 = len(grp1_abs)
        size_grp2 = len(grp2_abs)

        s_a.append([])

        for i in range(size_grp2):
            with tf.variable_scope(self.scope):
                s_a[-1].append(self.abs_cmb(grp1_abs[i], grp2_abs[i]))

        if size_grp1 != size_grp2:
            s_a.append(grp2_abs[-1])

        if len(s_a[-1]) == 1:
            return s_a
        else:
            return self.binary_tree_build(s_a)

class Abs_Dyn(object):
    def __init__(self, lock=False):
        self.dof = int(PRT_STATE_SIZE/2)
        self.dt = tf.constant(DT, dtype='float')
        self.h_layer = tf.keras.layers.Dense(
                ABS_DYN_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                self.dof,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None

    def __call__(self, s_p, s_a):
        with tf.variable_scope('AbsDyn' if self.scope is None else self.scope) as scope:
            comb_in = tf.concat((s_p, s_a), axis=1)
            p_old = s_p[:, 0:self.dof]
            v_old = s_p[:, self.dof:]
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            a_new = self.o_layer(h)
            v_new = v_old + self.dt*a_new
            p_new = p_old + self.dt*v_new
            o = tf.concat((p_new, v_new), axis=1)
            if self.scope is None:
                self.scope = scope
        return o

class Prt_Pol(object):
    def __init__(self, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                PRT_POL_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                ABS_STATE_SIZE,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None

    def __call__(self, s_p):
        with tf.variable_scope('PrtPol' if self.scope is None else self.scope) as scope:
            h = self.h_layer(s_p)
            h = self.dropout(h, training=TRAINING)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o
class Swm_Pol(object):
    def __init__(self, lock=False):
        self.h_layer = tf.keras.layers.Dense(
                SWM_POL_H_SIZE,
                tf.nn.leaky_relu,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='h')
        self.o_layer = tf.keras.layers.Dense(
                SWM_ACTIN_SIZE,
                use_bias=USE_BIAS,
                kernel_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                bias_regularizer=tf.keras.regularizers.l2(l=L2_GAIN),
                trainable=not lock,
                name='o')
        self.dropout = tf.keras.layers.Dropout(DROPOUT_RATE)
        self.scope = None

    def __call__(self, s_s, s_a, s_d, p_s):
        with tf.variable_scope('SwmPol' if self.scope is None else self.scope) as scope:
            err = tf.tile(tf.expand_dims(s_a-s_d, axis=1), (1, tf.shape(s_s)[1], 1))
            comb_in = tf.concat((s_s, err, p_s), axis=2)
            h = self.h_layer(comb_in)
            h = self.dropout(h, training=TRAINING)
            a_s = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return a_s

class System(object):
    theta_min = -np.pi/6.0
    theta_max = np.pi/6.0
    omega_min = -1.0
    omega_max = 1.0
    p_min = -0.375
    p_max = 0.375
    v_min = -1.0
    v_max = 1.0
    m_min = 1.5
    m_max = 2.0
    J = 1.5
    gamma_1 = 0.07
    gamma_2 = 1000
    gamma_3 = 700
    gamma_4 = 0.015
    gamma_5 = 1000
    gamma_6 = 0.5
    g = 9.81
    c = 1.5

    def __init__(self):
        with tf.variable_scope('DynSystem') as scope:
            self.J_tf = tf.constant(self.J, dtype='float')
            self.gamma_1_tf = tf.constant(self.gamma_1, dtype='float')
            self.gamma_2_tf = tf.constant(self.gamma_2, dtype='float')
            self.gamma_3_tf = tf.constant(self.gamma_3, dtype='float')
            self.gamma_4_tf = tf.constant(self.gamma_4, dtype='float')
            self.gamma_5_tf = tf.constant(self.gamma_5, dtype='float')
            self.gamma_6_tf = tf.constant(self.gamma_6, dtype='float')
            self.g_tf = tf.constant(self.g, dtype='float')
            self.c_tf = tf.constant(self.c, dtype='float')
            self.dt = tf.constant(DT, 'float')
            self.scope = scope

    def gen_prt_states(self, n):
        return rand_vectors(n, np.array([self.theta_min, self.omega_min]), np.array([self.theta_max, self.omega_max]))

    def gen_swm_states(self, n, swm_size):
        return rand_vectors((n, swm_size), np.array([self.p_min, self.v_min]), np.array([self.p_max, self.v_max]))

    def gen_swm_params(self, n, swm_size):
        return rand_vectors((n, swm_size), np.array([self.m_min]), np.array([self.m_max]))

    def strbk_friction(self, v):
        return self.gamma_1_tf*(tf.tanh(self.gamma_2_tf*v) - tf.tanh(self.gamma_3_tf)*v) + self.gamma_4_tf*tf.tanh(self.gamma_5_tf*v) + self.gamma_6_tf*v

    def prt_dyn_cont(self, s_p, s_s, p_s):
        m = p_s[:, :, 0]
        J_eff = self.J_tf + tf.reduce_sum(m*(s_s[:, :, 0]**2), axis=1)
        tau = tf.reduce_sum(m*s_s[:, :, 0], axis=1)
        J_dot = tf.reduce_sum(m*(s_s[:, :, 0]*s_s[:, :, 1]), axis=1)
        theta_dot = s_p[:, 1]
        omega_dot = (-self.g_tf*tf.cos(s_p[:, 0])*tau - s_p[:, 1]*J_dot - self.strbk_friction(s_p[:, 1]))/J_eff
        return tf.stack((theta_dot, omega_dot), axis=1)

    def swm_dyn_cont(self, s_s, a_s, p_s):
        m = p_s[:, :, 0]
        p_dot = s_s[:, :, 1]
        v_dot = (a_s[:, :, 0] - self.c_tf*s_s[:, :, 1])/m
        return tf.stack((p_dot, v_dot), axis=2)

    def dyn_RK4(self, s_p_old, s_s_old, a_s_old, p_s):
        with tf.variable_scope(self.scope):
            s_p_dot1 = self.prt_dyn_cont(s_p_old, s_s_old, p_s)
            s_s_dot1 = self.swm_dyn_cont(s_s_old, a_s_old, p_s)
            s_p_dot2 = self.prt_dyn_cont(s_p_old+0.5*self.dt*s_p_dot1, s_s_old+0.5*self.dt*s_s_dot1, p_s)
            s_s_dot2 = self.swm_dyn_cont(s_s_old+0.5*self.dt*s_s_dot1, a_s_old, p_s)
            s_p_dot3 = self.prt_dyn_cont(s_p_old+0.5*self.dt*s_p_dot2, s_s_old+0.5*self.dt*s_s_dot2, p_s)
            s_s_dot3 = self.swm_dyn_cont(s_s_old+0.5*self.dt*s_s_dot2, a_s_old, p_s)
            s_p_dot4 = self.prt_dyn_cont(s_p_old+self.dt*s_p_dot3, s_s_old+self.dt*s_s_dot3, p_s)
            s_s_dot4 = self.swm_dyn_cont(s_s_old+self.dt*s_s_dot3, a_s_old, p_s)

            s_p_new = s_p_old + self.dt/6.0*(s_p_dot1 + 2*s_p_dot2 + 2*s_p_dot3 + s_p_dot4)
            s_s_new = s_s_old + self.dt/6.0*(s_s_dot1 + 2*s_s_dot2 + 2*s_s_dot3 + s_s_dot4)

        return s_p_new, s_s_new

    def dyn_SIE(self, s_p_old, s_s_old, a_s, p_s):
        theta_old = s_p_old[:, 0]
        omega_old = s_p_old[:, 1]

        p_old = s_s_old[:, :, 0]
        v_old = s_s_old[:, :, 1]

        f = a_s[:, :, 0]

        m = p_s[:, :, 0]

        tau = tf.reduce_sum(m*p_old, axis=1)
        J = tf.reduce_sum(m*p_old**2, axis=1) + self.J_tf
        J_dot = tf.reduce_sum(2*m*p_old*v_old, axis=1)

        alpha_new = (-self.g_tf*tf.cos(theta_old)*tau - omega_old*J_dot - self.strbk_friction(omega_old))/J
        omega_new = omega_old + self.dt*alpha_new
        theta_new = theta_old + self.dt*omega_new

        a_new = (f - self.c_tf*v_old)/m
        v_new = v_old + self.dt*a_new
        p_new = p_old + self.dt*v_new

        s_p_new = tf.stack((theta_new, omega_new), axis=1)
        s_s_new = tf.stack((p_new, v_new), axis=2)

        return s_p_new, s_s_new

    def __call__(self, s_p_old, s_s_old, a_s_old, p_s):
        return self.dyn_SIE(s_p_old, s_s_old, a_s_old, p_s)


class Iteration(object):
    def __init__(self, abs_enc, abs_dyn, prt_pol, swm_pol, sys):
        self.abs_enc = abs_enc
        self.abs_dyn = abs_dyn
        self.prt_pol = prt_pol
        self.swm_pol = swm_pol
        self.sys = sys

    def __call__(self, s_p_old, s_s_old, p_s, swm_size):
        with tf.variable_scope('Iter'):
            s_a_old = self.abs_enc(s_s_old, p_s, swm_size)

            s_d_old = self.prt_pol(s_p_old)
            a_s_old = self.swm_pol(s_s_old, s_a_old, s_d_old, p_s)

            s_p_new, s_s_new = self.sys(s_p_old, s_s_old, a_s_old, p_s)

            s_p_prd = self.abs_dyn(s_p_old, s_a_old)

        return s_p_new, s_s_new, s_a_old, s_d_old, a_s_old, s_p_prd

class TASC_trainer(object):
    def __init__(self):
        self.abs_enc = Abs_Enc()
        self.abs_dyn = Abs_Dyn()
        self.prt_pol = Prt_Pol()
        self.swm_pol = Swm_Pol()
        self.sys = System()
        self.iter = Iteration(self.abs_enc, self.abs_dyn, self.prt_pol, self.swm_pol, self.sys)
        self.loss_scope = None
        self.build()

    def build(self):
        with tf.variable_scope('TASC_Learner') as scope:
            self.scope = scope
            self.s_p_0 = tf.placeholder('float', (None, PRT_STATE_SIZE), 's_p_0')
            self.s_s_0 = tf.placeholder('float', (None, None, SWM_STATE_SIZE), 's_s_0')
            self.p_s = tf.placeholder('float', (None, None, SWM_PARAM_SIZE), 'p_s')

            Q_p = tf.constant(Q_PRT, dtype='float')
            R_s = tf.constant(R_SWM, dtype='float')
            L  = tf.constant((self.sys.p_max - self.sys.p_min), dtype='float')

            self.outputs = []
            self.losses = []
            self.trainers = []
            self.train_ops = []

            self.swm_sizes = [2**(i+1) for i in range(int(math.ceil(math.log(SWM_MAXIM_SIZE, 2))))]

            for swm_size in self.swm_sizes:
                print('building network for swarm size %d'%swm_size)
                s_p = [self.s_p_0]
                s_s = [self.s_s_0]
                s_a = []
                s_d = []
                a_s = []
                l_abs = []
                l_swm = []
                l_prt = []
                l_bnd = []

                for i in trange(N):
                    s_p_nxt, s_s_nxt, s_a_cur, s_d_cur, a_s_cur, s_p_prd = self.iter(s_p[i], s_s[i], self.p_s, swm_size)

                    s_p.append(s_p_nxt)
                    s_s.append(s_s_nxt)
                    s_a.append(s_a_cur)
                    s_d.append(s_d_cur)
                    a_s.append(a_s_cur)

                    with tf.variable_scope('loss' if self.loss_scope is None else self.loss_scope) as scope:
                        l_abs.append(GAMMA_ABS*tf.reduce_sum(tf.squared_difference(s_p_nxt, s_p_prd), axis=1))
                        l_swm.append(GAMMA_SWM*tf.reduce_sum(tf.squared_difference(s_a_cur, s_d_cur), axis=1) + tf.einsum('ijk,kl,ijl->i', a_s_cur, R_s, a_s_cur))
                        l_prt.append(tf.einsum('ij,jk,ik->i', s_p_nxt, Q_p, s_p_nxt))
                        l_bnd.append(GAMMA_BND*tf.reduce_sum(tf.square(tf.maximum(0.0, tf.abs(s_s_nxt[:, :, 0])-L/2.0)), axis=1))

                        if self.loss_scope is None:
                            self.loss_scope = scope

                with tf.variable_scope('outputs'):
                    self.outputs.append({})
                    self.outputs[-1]['s_p'] = tf.stack(s_p, axis=1)
                    self.outputs[-1]['s_s'] = tf.stack(s_s, axis=1)
                    self.outputs[-1]['s_a'] = tf.stack(s_a, axis=1)
                    self.outputs[-1]['s_d'] = tf.stack(s_d, axis=1)
                    self.outputs[-1]['a_s'] = tf.stack(a_s, axis=1)

                with tf.variable_scope(self.loss_scope):
                    self.outputs[-1]['l_abs'] = tf.stack(l_abs, axis=1)
                    self.outputs[-1]['l_swm'] = tf.stack(l_swm, axis=1)
                    self.outputs[-1]['l_prt'] = tf.stack(l_prt, axis=1)
                    self.outputs[-1]['l_bnd'] = tf.stack(l_bnd, axis=1)

                    self.losses.append({})
                    self.losses[-1]['L_abs'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_abs'], axis=1))
                    self.losses[-1]['L_swm'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_swm'], axis=1))
                    self.losses[-1]['L_prt'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_prt'], axis=1))
                    self.losses[-1]['L_bnd'] = tf.reduce_mean(tf.reduce_sum(self.outputs[-1]['l_bnd'], axis=1))

                    total_loss = self.losses[-1]['L_abs'] + self.losses[-1]['L_swm'] + self.losses[-1]['L_prt'] + self.losses[-1]['L_bnd'] + tf.losses.get_regularization_loss()

                with tf.variable_scope('train', reuse=tf.AUTO_REUSE):
                    self.trainers.append(tf.train.AdamOptimizer(learning_rate=LR))
                    self.train_ops.append(self.trainers[-1].minimize(total_loss))

    def train(self):
        hist = {}

        SESS.run(tf.global_variables_initializer())

        for key in self.losses[0].keys():
            hist['ave_%s'%key] = np.zeros(N_EPOCH)
            hist['val_%s'%key] = np.zeros(N_EPOCH)

        min_weights=[]
        min_loss = math.inf
        min_loss_epoch = -1
        min_count = 0
        var_list = tf.trainable_variables()

        val_blocks = []
        for swm_size_i in range(len(self.swm_sizes)):
            for i in range(VAL_BLOK):
                val_blocks.append({})
                swm_size = np.random.randint(2**swm_size_i+1, 2**(swm_size_i+1)+1)
                val_blocks[-1]['s_p_0'] = self.sys.gen_prt_states(VAL_SIZE)
                val_blocks[-1]['s_s_0'] = self.sys.gen_swm_states(VAL_SIZE, swm_size)
                val_blocks[-1]['p_s'] = self.sys.gen_swm_params(VAL_SIZE, swm_size)
                val_blocks[-1]['swm_i'] = swm_size_i
                val_blocks[-1]['feed_dict'] = {self.s_p_0:val_blocks[-1]['s_p_0'], self.s_s_0:val_blocks[-1]['s_s_0'], self.p_s:val_blocks[-1]['p_s'], TRAINING:False}

        for epoch in range(N_EPOCH):
            print('Epoch: %d/%d'%(epoch+1, N_EPOCH))
            batches = trange(N_BATCH)
            for batch in batches:
                if batch%SWM_SIZE_UPDATE_RATE == 0:
                    swm_size_i = np.random.randint(len(self.swm_sizes))
                swm_size = np.random.randint(2**swm_size_i+1, 2**(swm_size_i+1)+1)
                s_p_0_bat = self.sys.gen_prt_states(BAT_SIZE)
                s_s_0_bat = self.sys.gen_swm_states(BAT_SIZE, swm_size)
                p_s_bat = self.sys.gen_swm_params(BAT_SIZE, swm_size)
                feed_dict_bat = {self.s_p_0:s_p_0_bat, self.s_s_0:s_s_0_bat, self.p_s:p_s_bat, TRAINING:True}

                losses, _ = SESS.run([self.losses[swm_size_i], self.train_ops[swm_size_i]], feed_dict=feed_dict_bat)

                msg=''
                for key in self.losses[swm_size_i].keys():
                    hist['ave_%s'%key][epoch] += losses[key]
                    msg += '(ave_%s: %f)'%(key, hist['ave_%s'%key][epoch]/(batch+1))

                batches.set_description(msg)

            val_losses = []
            val_outputs = []
            for block in val_blocks:
                l, o = SESS.run([self.losses[block['swm_i']], self.outputs[block['swm_i']]], feed_dict=block['feed_dict'])
                val_losses.append(l)
                val_outputs.append(o)

            total_loss = 0
            msg = ''
            for key in self.losses[0].keys():
                hist['ave_%s'%key][epoch] /= N_BATCH
                for val_loss in val_losses:
                    hist['val_%s'%key][epoch] += val_loss[key]
                hist['val_%s'%key][epoch] /= len(val_losses)
                total_loss += hist['val_%s'%key][epoch]
                msg += '(%s Ave: %f, Val: %f)'%(key, hist['ave_%s'%key][epoch], hist['val_%s'%key][epoch])

            print('Epoch %d Complete! %s'%(epoch+1, msg))

            if total_loss < min_loss:
                min_loss = total_loss
                min_weights = SESS.run(var_list)
                min_count = 0
                min_loss_epoch = epoch
                if AUTOSAVE_FILENAME is not None:
                    self.save(AUTOSAVE_FILENAME%epoch, to_save=(var_list, min_weights))
                if VAL_RSLT_FILENAME is not None:
                    s_p_0 = []
                    s_s_0 = []
                    p_s = []
                    for block in val_blocks:
                        s_p_0.append(block['s_p_0'])
                        s_s_0.append(block['s_s_0'])
                        p_s.append(block['p_s'])
                    pkl.dump([val_outputs, val_losses, s_p_0, s_s_0, p_s], open(HOME+VAL_RSLT_FILENAME%epoch, 'wb'))
            else:
                min_count += 1

            pkl.dump(hist, open(HOME+HST_RSLT_FILENAME, 'wb'))

            if min_count == PATIENCE:
                print('Stopping Training, validation loss has stopped decreasing')
                for key in self.losses[0].keys():
                    hist['ave_%s'%key] = hist['ave_%s'%key][:epoch+1]
                    hist['val_%s'%key] = hist['val_%s'%key][:epoch+1]
                break

        SESS.run([var.assign(val) for var, val in zip(var_list, min_weights)])
        hist['min_loss_epoch'] = min_loss_epoch
        return hist

    def save(self, filename, to_save=None):
        if to_save is None:
            var_list = tf.trainable_variables()
            val_list = SESS.run(var_list)
        else:
            var_list, val_list = to_save

        os.makedirs(HOME, exist_ok=True)
        pkl.dump({var.name:val for var, val in zip(var_list, val_list)}, open(HOME+filename, 'wb'))

    def load(self, filename):
        var_list = tf.trainable_variables()
        vals = pkl.load(open(HOME+filename, 'rb'))

        SESS.run([var.assign(vals[var.name]) for var in var_list])

if __name__ == '__main__':
    os.makedirs(HOME, exist_ok=True)
    os.makedirs(LOGS, exist_ok=True)
    print('Setting up networks...')
    learner = TASC_trainer()

    #print('Logging...')
    #tf.summary.FileWriter(LOGS, SESS.graph)

    print('Training...')
    hist = learner.train()

    print('Saving results')
    learner.save(FINAL_FILENAME)
