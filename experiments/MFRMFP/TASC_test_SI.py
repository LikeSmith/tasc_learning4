"""
TASC_test.py
"""

import tensorflow as tf
import numpy as np
from tqdm import trange
import _pickle as pkl
import matplotlib.pyplot as plt
import math

import TASC_Learner_SI as exp

SWM_SIZE = 16

Q_p = np.array([[10.0, 0.0], [0.0, 1.0]])
R_s = np.array([[0.1]])
GAMMA_LQR = 1.0

FILENAME = 'final_weights.pkl'

class Tester(object):
    def __init__(self):
        self.abs_trn = exp.Abs_Trn()
        self.abs_cmb = exp.Abs_Cmb()
        self.prt_pol = exp.Prt_Pol()
        self.swm_pol = exp.Swm_Pol()
        self.sys = exp.System()
        self.build()


    def build(self):
        with tf.variable_scope('TASC_Learner/Iter') as scope:
            self.scope = scope
            self.s_p_cur = tf.placeholder('float', (None, exp.PRT_STATE_SIZE), 's_p_cur')
            self.s_s_cur = tf.placeholder('float', (None, None, exp.SWM_STATE_SIZE), 's_s_cur')
            self.s_s_sng = tf.placeholder('float', (None, exp.SWM_STATE_SIZE), 's_s_sng')
            self.p_s = tf.placeholder('float', (None, None, exp.SWM_PARAM_SIZE), 'p_s')
            self.p_s_sng = tf.placeholder('float', (None, exp.SWM_PARAM_SIZE), 'p_s_sng')

            self.s_a1 = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_a1')
            self.s_a2 = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_a1')

            self.s_a_cur = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_a_cur')
            self.s_d_cur = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_d_cur')

            self.a_s_cur = tf.placeholder('float', (None, None, exp.SWM_ACTIN_SIZE), 'a_s_cur')

            with tf.variable_scope('AbsEnc'):
                self.s_a_sng = self.abs_trn(self.s_s_sng, self.p_s_sng)
            with tf.variable_scope('AbsEnc_1'):
                self.s_a_cmb = self.abs_cmb(self.s_a1, self.s_a2)

            self.s_d_out = self.prt_pol(self.s_p_cur)

            self.a_s_out = self.swm_pol(self.s_s_cur, self.s_a_cur, self.s_d_cur, self.p_s)

            self.s_p_nxt, self.s_s_nxt = self.sys(self.s_p_cur, self.s_s_cur, self.a_s_cur, self.p_s)

    def load(self, filename):
        var_list = tf.trainable_variables()
        vals = pkl.load(open(exp.HOME+filename, 'rb'))

        exp.SESS.run([var.assign(vals[var.name]) for var in var_list])

    def apply_abs_enc(self, s_s, p_s):
        pad = [[0, 0], [0, 2**math.ceil(math.log(s_s.shape[1], 2)) - s_s.shape[1]], [0, 0]]
        s_s = np.pad(s_s, pad, 'constant')
        p_s = np.pad(p_s, pad, 'constant')
        s_a = [[]]
        for i in range(s_s.shape[1]):
            s_a[0].append(exp.SESS.run(self.s_a_sng, feed_dict={self.s_s_sng:s_s[:, i, :], self.p_s_sng:p_s[:, i, :]}))

        def binary_build(s_a):
            grp1_a = s_a[-1][0::2]
            grp2_a = s_a[-1][1::2]

            s_a.append([])
            for i in range(len(grp2_a)):
                s_a[-1].append(exp.SESS.run(self.s_a_cmb, feed_dict={self.s_a1:grp1_a[i], self.s_a2:grp2_a[i]}))

            if len(s_a[-1]) == 1:
                return s_a
            else:
                return binary_build(s_a)

        s_a = binary_build(s_a)
        return s_a[-1][0]

    def apply_prt_pol(self, s_p):
        return exp.SESS.run(self.s_d_out, feed_dict={self.s_p_cur:s_p})

    def apply_swm_pol(self, s_s, s_a, s_d, p_s):
        return exp.SESS.run(self.a_s_out, feed_dict={self.s_s_cur:s_s, self.s_a_cur:s_a, self.s_d_cur:s_d, self.p_s:p_s})

    def apply_dyn(self, s_p, s_s, a_s, p_s):
        return exp.SESS.run([self.s_p_nxt, self.s_s_nxt], feed_dict={self.s_p_cur:s_p, self.s_s_cur:s_s, self.a_s_cur:a_s, self.p_s:p_s})

    def run_sim(self, s_p0, s_s0, p_s, t_f):
        t = np.arange(0, t_f+exp.DT, exp.DT)
        s_p = np.zeros((s_p0.shape[0], t.shape[0], exp.PRT_STATE_SIZE))
        s_s = np.zeros((s_s0.shape[0], t.shape[0], s_s0.shape[1], exp.SWM_STATE_SIZE))
        s_a = np.zeros((s_s0.shape[0], t.shape[0]-1, exp.ABS_STATE_SIZE))
        s_d = np.zeros((s_s0.shape[0], t.shape[0]-1, exp.ABS_STATE_SIZE))
        a_s = np.zeros((s_s0.shape[0], t.shape[0]-1, s_s0.shape[1], exp.SWM_ACTIN_SIZE))
        l_lqr = np.zeros((s_s0.shape[0], t.shape[0]))

        s_p[:, 0, :] = s_p0
        s_s[:, 0, :, :] = s_s0

        for i in trange(t.shape[0]-1):
            s_a[:,i,:] = self.apply_abs_enc(s_s[:,i,:,:], p_s)
            s_d[:,i,:] = self.apply_prt_pol(s_p[:,i,:])
            a_s[:,i,:,:] = self.apply_swm_pol(s_s[:,i,:,:], s_a[:,i,:], s_d[:,i,:], p_s)
            s_p[:,i+1,:], s_s[:,i+1,:,:] = self.apply_dyn(s_p[:,i,:], s_s[:,i,:,:], a_s[:,i,:,:], p_s)

            l_lqr[:, i] = np.einsum('ij,jk,ik->i', s_p[:, i, :], Q_p, s_p[:, i, :])*exp.DT + np.einsum('ijk,kl,ijl->i', a_s[:, i, :, :], R_s, a_s[:, i, :, :])*exp.DT/SWM_SIZE + np.sum(np.maximum(0, np.abs(s_s[:, i, :, 0] - 0.375))**2, axis=1)*exp.DT/SWM_SIZE

        L_lqr = np.mean(np.sum(l_lqr, axis=1))

        return t, s_p, s_s, s_a, s_d, a_s, L_lqr

    def gen_init_states(self, n, swarm_size):
        s_p = self.sys.gen_prt_states(n)
        s_s = self.sys.gen_swm_states(n, swarm_size)
        p_s = self.sys.gen_swm_params(n, swarm_size)
        return s_p, s_s, p_s

if __name__ == '__main__':
    print('loading networks...')
    tester = Tester()
    tester.load(FILENAME)

    print('running sim...')
    s_p0, s_s0, p_s = tester.gen_init_states(100, SWM_SIZE)
    t, s_p, s_s, s_a, s_d, a_s, L_lqr = tester.run_sim(s_p0, s_s0, p_s, 10.0)

    print('L_lqr: %f'%L_lqr)

    plt.figure()
    plt.plot(t, s_p[0, :, :])
    plt.xlabel('time (s)')
    plt.legend(['theta (rad)', 'theta dot (rad/s)'])

    plt.figure()
    plt.plot(t, s_s[0, :, :, 0])
    plt.xlabel('time (s)')
    plt.ylabel('pos (m)')
