"""
TASC_test.py
"""

import tensorflow as tf
import numpy as np
from tqdm import trange
import _pickle as pkl
import matplotlib.pyplot as plt
import math

import TASC_Learner_noHomo as exp

SWM1_SIZE = 4
SWM2_SIZE = 2
TF = 10.0

Q_p = np.array([[10.0, 0.0], [0.0, 1.0]])
R_s = np.array([[0.1]])
GAMMA_LQR = 1.0

FILENAME = 'autosave_epoch12.pkl'

class Tester(object):
    def __init__(self):
        self.abs_trn1 = exp.Abs_Trn()
        self.abs_trn2 = exp.Abs_Trn()
        self.abs_cmb = exp.Abs_Cmb()
        self.prt_pol = exp.Prt_Pol()
        self.swm1_pol = exp.Swm_Pol(exp.SWM1_ACTIN_SIZE)
        self.swm2_pol = exp.Swm_Pol(exp.SWM2_ACTIN_SIZE)
        self.sys = exp.System()
        self.build()


    def build(self):
        with tf.variable_scope('TASC_Learner/Iter') as scope:
            self.scope = scope
            self.s_p_cur = tf.placeholder('float', (None, exp.PRT_STATE_SIZE), 's_p_cur')
            self.s_s1_cur = tf.placeholder('float', (None, None, exp.SWM1_STATE_SIZE), 's_s1_cur')
            self.s_s2_cur = tf.placeholder('float', (None, None, exp.SWM2_STATE_SIZE), 's_s2_cur')
            self.p_s1 = tf.placeholder('float', (None, None, exp.SWM1_PARAM_SIZE), 'p_s1')
            self.p_s2 = tf.placeholder('float', (None, None, exp.SWM2_PARAM_SIZE), 'p_s2')

            self.s_a1_cmb = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_a1')
            self.s_a2_cmb = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_a2')

            self.s_a_cur = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_a_cur')
            self.s_d_cur = tf.placeholder('float', (None, exp.ABS_STATE_SIZE), 's_d_cur')

            self.a_s1_cur = tf.placeholder('float', (None, None, exp.SWM1_ACTIN_SIZE), 'a_s1_cur')
            self.a_s2_cur = tf.placeholder('float', (None, None, exp.SWM2_ACTIN_SIZE), 'a_s2_cur')

            with tf.variable_scope('AbsEnc'):
                with tf.variable_scope('Swm1'):
                    self.s_a1 = self.abs_trn1(self.s_s1_cur, self.p_s1)
                with tf.variable_scope('Swm2'):
                    self.s_a2 = self.abs_trn2(self.s_s2_cur, self.p_s2)
            with tf.variable_scope('AbsEnc_1'):
                self.s_a_cmb = self.abs_cmb(self.s_a1_cmb, self.s_a2_cmb)

            self.s_d_out = self.prt_pol(self.s_p_cur)

            with tf.variable_scope('Swm1'):
                self.a_s1_out = self.swm1_pol(self.s_s1_cur, self.s_a_cur, self.s_d_cur, self.p_s1)
            with tf.variable_scope('Swm2'):
                self.a_s2_out = self.swm2_pol(self.s_s2_cur, self.s_a_cur, self.s_d_cur, self.p_s2)

            self.s_p_nxt, self.s_s1_nxt, self.s_s2_nxt = self.sys(self.s_p_cur, self.s_s1_cur, self.s_s2_cur, self.a_s1_cur, self.a_s2_cur, self.p_s1, self.p_s2)

    def load(self, filename):
        var_list = tf.trainable_variables()
        vals = pkl.load(open(exp.HOME+filename, 'rb'))

        exp.SESS.run([var.assign(vals[var.name]) for var in var_list])

    def apply_abs_enc(self, s_s1, s_s2, p_s1, p_s2):
        pad_size = 2**math.ceil(math.log(s_s1.shape[1]+s_s2.shape[1], 2)) - s_s1.shape[1] - s_s2.shape[1]
        s_a1 = exp.SESS.run(self.s_a1, feed_dict={self.s_s1_cur:s_s1, self.p_s1:p_s1})
        s_a2 = exp.SESS.run(self.s_a2, feed_dict={self.s_s2_cur:s_s2, self.p_s2:p_s2})
        s_a0 = exp.SESS.run(self.s_a2, feed_dict={self.s_s2_cur:np.zeros((s_s2.shape[0], pad_size, s_s2.shape[2])), self.p_s2:np.zeros((p_s2.shape[0], pad_size, p_s2.shape[2]))})

        s_a = [[]]
        for i in range(s_a1.shape[1]):
            s_a[0].append(s_a1[:, i, :])
        for i in range(s_a2.shape[1]):
            s_a[0].append(s_a2[:, i, :])
        for i in range(s_a0.shape[1]):
            s_a[0].append(s_a0[:, i, :])

        def binary_build(s_a):
            grp1_a = s_a[-1][0::2]
            grp2_a = s_a[-1][1::2]

            s_a.append([])

            for i in range(len(grp2_a)):
                s_a[-1].append(exp.SESS.run(self.s_a_cmb, feed_dict={self.s_a1_cmb:grp1_a[i], self.s_a2_cmb:grp2_a[i]}))

            if len(s_a[-1]) == 1:
                return s_a
            else:
                return binary_build(s_a)

        s_a = binary_build(s_a)
        return s_a[-1][0]

    def apply_prt_pol(self, s_p):
        return exp.SESS.run(self.s_d_out, feed_dict={self.s_p_cur:s_p})

    def apply_swm_pol(self, s_s1, s_s2, s_a, s_d, p_s1, p_s2):
        a_s1 = exp.SESS.run(self.a_s1_out, feed_dict={self.s_s1_cur:s_s1, self.s_a_cur:s_a, self.s_d_cur:s_d, self.p_s1:p_s1})
        a_s2 = exp.SESS.run(self.a_s2_out, feed_dict={self.s_s2_cur:s_s2, self.s_a_cur:s_a, self.s_d_cur:s_d, self.p_s2:p_s2})
        return a_s1, a_s2

    def apply_dyn(self, s_p, s_s1, s_s2, a_s1, a_s2, p_s1, p_s2):
        return exp.SESS.run([self.s_p_nxt, self.s_s1_nxt, self.s_s2_nxt], feed_dict={self.s_p_cur:s_p, self.s_s1_cur:s_s1, self.s_s2_cur:s_s2, self.a_s1_cur:a_s1, self.a_s2_cur:a_s2, self.p_s1:p_s1, self.p_s2:p_s2})

    def run_sim(self, s_p0, s_s10, s_s20, p_s1, p_s2, t_f):
        t = np.arange(0, t_f+exp.DT, exp.DT)
        s_p = np.zeros((s_p0.shape[0], t.shape[0], exp.PRT_STATE_SIZE))
        s_s1 = np.zeros((s_s10.shape[0], t.shape[0], s_s10.shape[1], exp.SWM1_STATE_SIZE))
        s_s2 = np.zeros((s_s20.shape[0], t.shape[0], s_s20.shape[1], exp.SWM2_STATE_SIZE))
        s_a = np.zeros((s_p0.shape[0], t.shape[0]-1, exp.ABS_STATE_SIZE))
        s_d = np.zeros((s_p0.shape[0], t.shape[0]-1, exp.ABS_STATE_SIZE))
        a_s1 = np.zeros((s_s10.shape[0], t.shape[0]-1, s_s10.shape[1], exp.SWM1_ACTIN_SIZE))
        a_s2 = np.zeros((s_s20.shape[0], t.shape[0]-1, s_s20.shape[1], exp.SWM2_ACTIN_SIZE))
        l_lqr = np.zeros((s_p0.shape[0], t.shape[0]))

        s_p[:,0,:] = s_p0
        s_s1[:,0,:,:] = s_s10
        s_s2[:,0,:,:] = s_s20

        for i in trange(t.shape[0]-1):
            s_a[:,i,:] = self.apply_abs_enc(s_s1[:,i,:,:], s_s2[:,i,:,:], p_s1, p_s2)
            s_d[:,i,:] = self.apply_prt_pol(s_p[:,i,:])
            a_s1[:,i,:,:], a_s2[:,i,:,:] = self.apply_swm_pol(s_s1[:,i,:,:], s_s2[:,i,:,:], s_a[:,i,:], s_d[:,i,:], p_s1, p_s2)
            s_p[:,i+1,:], s_s1[:,i+1,:,:], s_s2[:,i+1,:,:] = self.apply_dyn(s_p[:,i,:], s_s1[:,i,:,:], s_s2[:,i,:,:], a_s1[:,i,:,:], a_s2[:,i,:,:], p_s1, p_s2)

            l_lqr[:, i] = np.einsum('ij,jk,ik->i', s_p[:, i, :], Q_p, s_p[:, i, :])*exp.DT + np.einsum('ijk,kl,ijl->i', a_s1[:, i, :, :], R_s, a_s1[:, i, :, :])*exp.DT/SWM1_SIZE + np.sum(np.maximum(0, np.abs(s_s1[:, i, :, 0] - 0.375))**2, axis=1)*exp.DT/SWM1_SIZE + np.einsum('ijk,kl,ijl->i', a_s2[:, i, :, :], R_s, a_s2[:, i, :, :])*exp.DT/SWM2_SIZE + np.sum(np.maximum(0, np.abs(s_s2[:, i, :, 0] - 0.375))**2, axis=1)*exp.DT/SWM2_SIZE

        L_lqr = np.mean(np.sum(l_lqr, axis=1))

        return t, s_p, s_s1, s_s2, s_a, s_d, a_s1, a_s2, L_lqr

    def gen_init_states(self, n, swarm1_size, swarm2_size):
        s_p = self.sys.gen_prt_states(n)
        s_s1 = self.sys.gen_swm1_states(n, swarm1_size)
        s_s2 = self.sys.gen_swm2_states(n, swarm2_size)
        p_s1 = self.sys.gen_swm1_params(n, swarm1_size)
        p_s2 = self.sys.gen_swm2_params(n, swarm2_size)
        return s_p, s_s1, s_s2, p_s1, p_s2

if __name__ == '__main__':
    print('loading networks...')
    tester = Tester()
    tester.load(FILENAME)

    print('running sim...')
    s_p0, s_s10, s_s20, p_s1, p_s2 = tester.gen_init_states(1, SWM1_SIZE, SWM2_SIZE)
    t, s_p, s_s1, s_s2, s_a, s_d, a_s1, a_s2, L_lqr = tester.run_sim(s_p0, s_s10, s_s20, p_s1, p_s2, TF)

    print('L_lqr: %f'%L_lqr)

    plt.figure()
    plt.plot(t, s_p[0, :, :])
    plt.xlabel('time (s)')
    plt.legend(['theta (rad)', 'theta dot (rad/s)'])

    plt.figure()
    plt.plot(t, s_s1[0, :, :, 0])
    plt.plot(t, s_s2[0, :, :, 0])
    plt.xlabel('time (s)')
    plt.ylabel('pos (m)')
